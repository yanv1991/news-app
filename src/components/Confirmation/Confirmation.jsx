import React from 'react';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import WarningIcon from '@material-ui/icons/Warning';
import amber from '@material-ui/core/colors/amber';

const styles = theme => ({
  root: {
    position: 'relative',
    overflow: 'hidden',
  },
  appFrame: {
    width: 360,
    height: 360,
    backgroundColor: theme.palette.background.paper,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  button: {
    marginBottom: theme.spacing.unit,
  },
  fab: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  },
  fabMoveUp: {
    transform: 'translate3d(0, -46px, 0)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.enteringScreen,
      easing: theme.transitions.easing.easeOut,
    }),
  },
  fabMoveDown: {
    transform: 'translate3d(0, 0, 0)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.leavingScreen,
      easing: theme.transitions.easing.sharp,
    }),
  },
  snackbar: {
    position: 'absolute',
  },
  snackbarContent: {
    width: 360,
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

class Confirmation extends React.Component {

  handleConfirm = () => {
    const { toggleConfirmation, handleConfirm, id } = this.props

    handleConfirm(id)
    toggleConfirmation()
  };

  render() {
    const { showConfirmation, classes, toggleConfirmation } = this.props
    return (
      <Snackbar
        open={showConfirmation}
        autoHideDuration={null}
        onClose={toggleConfirmation}
        ContentProps={{
          'aria-describedby': 'snackbar-fab-message-id',
          className: classes.snackbarContent,
        }}
        message={
          <span id="snackbar-fab-message-id" className={classes.message}>
            <WarningIcon className={classNames(classes.icon, classes.iconVariant)} />
            Are you sure?
          </span>
        }
        action={[
          <Button key="yesButton" color="inherit" size="small" onClick={this.handleConfirm}>
            Yes
          </Button>,
          <Button key="noButton" color="inherit" size="small" onClick={toggleConfirmation}>
            No
          </Button>,
        ]}
        className={classes.snackbar}
      />
    );
  }
}

export default withStyles(styles)(Confirmation);
