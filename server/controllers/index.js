import express from 'express';

import serverRenderer from '../middleware/renderer';
import configureStore from '../../src/store';
import { fetchNews } from '../../src/actions/news'
import { loginAuthSSR } from '../../src/actions/users'

const path = require('path');
const router = express.Router();

// root (/) should always serve our server rendered page
const actionIndex = (req, res, next) => {
    const store = configureStore();

    store.dispatch(loginAuthSSR(req.headers.cookie))
    store.dispatch(fetchNews())
    .then(() => {
        serverRenderer(store)(req, res, next);
    });
};

router.use('^/$', actionIndex);

// other static resources should just be served as they are
router.use(express.static(
    path.resolve(__dirname, '..', '..', 'build'),
    { maxAge: '30d' },
));

export default router
