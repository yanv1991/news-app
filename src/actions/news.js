import fetch from 'cross-fetch';

import { GET_NEWS, GET_NEWS_SUCCESS, UPDATE_NEW_SUCCESS, DELETE_NEW_SUCCESS, CREATE_NEW_SUCCESS } from '../constants/ActionTypes'
import { ENDPOINT_URL } from '../constants'
import { getAuthorization } from '../helpers'

function requestNews() {
  return {
    type: GET_NEWS,
  }
}

function receiveNews(payload) {
  return {
    type: GET_NEWS_SUCCESS,
    payload,
  }
}

function receiveUpdatedNew(payload) {
  return {
    type: UPDATE_NEW_SUCCESS,
    payload,
  }
}

function receiveCreatedNew(payload) {
  return {
    type: CREATE_NEW_SUCCESS,
    payload,
  }
}

function deleteNewSucess(payload) {
  return {
    type: DELETE_NEW_SUCCESS,
    payload,
  }
}

export function fetchNews() {
  return function(dispatch) {
    dispatch(requestNews())

    return fetch(`${ENDPOINT_URL}/news`)
      .then((res) => res.json())
      .then(json => {
        return dispatch(receiveNews(json))
      }).catch((err) => { console.error('err', err) })
  }
}

export function updateNew(item) {
  return function(dispatch) {

    const formData = getFormData(item)
    const Authorization = getAuthorization()

    return fetch(`${ENDPOINT_URL}/news/${item._id}`, {
        method: 'PUT',
        body: formData,
        headers: { Authorization },
      })
      .then((res) => res.json())
      .then(json => {
        return dispatch(receiveUpdatedNew(json))
      }).catch((err) => { return { err }})
  }
}

function getFormData(item) {
  const formData = new FormData();

  if(item.image && item.image.length) {
    formData.append('image', item.image[0]);
  }

  formData.append('name', item.name);
  formData.append('description', item.description);
  formData.append('user', item.userId);

  return formData
}

export function createNew(item) {
  return function(dispatch) {
    const formData = getFormData(item)
    const Authorization = getAuthorization()

    return fetch(`${ENDPOINT_URL}/news`, {
        method: 'POST',
        body: formData,
        headers: { Authorization },
      })
      .then((res) => res.json())
      .then(json => {
        return dispatch(receiveCreatedNew(json))
      }).catch((err) => { return { err }})
  }
}

export function deleteNew(id) {
  return function(dispatch) {
    const Authorization = getAuthorization()

    return fetch(`${ENDPOINT_URL}/news/${id}`, { method: 'DELETE', headers: { Authorization }})
      .then(() => {
        return dispatch(deleteNewSucess(id))
      }).catch((err) => {  return { err }})
  }
}
