import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'

import reducers from '../reducers'

const configureStore = (preloadedState) => {
  const rootReducer = combineReducers(reducers)

  const store = createStore(rootReducer, preloadedState, applyMiddleware(...[thunk, logger]))

  return store
}

export default configureStore
