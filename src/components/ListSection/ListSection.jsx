import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    marginTop: '10px'
  },
});

function ListSection(props) {
  const { classes, items } = props;
  return (
    <List component="nav" className={classes.root}>
      {
        items.map((item) => (
          <ListItem key="item" button>
            <ListItemText className={classes.text} inset primary={item} />
          </ListItem>
        ))
      }
    </List>
  );
}

ListSection.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListSection);
