import { LOGIN_SUCCESS, LOGOUT_SUCCESS, LOGIN_FAIL, LOAD_AUTH_SSR } from '../constants/ActionTypes'
import { getCookies, getSession } from '../helpers'

const cookies = getCookies()
const { user, isExpired } = getSession(cookies)

const initialState = {
  userId: user && !isExpired ? user._id : '',
  name: user && !isExpired ? user.name : {}
}

const handlers = {
  [LOAD_AUTH_SSR]: (state, { cookies: ssrCookie }) => loadTokenSSR(state, ssrCookie),
  [LOGIN_FAIL]: (state) => ({ ...state, error: 'Login Failed' }),
  [LOGOUT_SUCCESS]: (state) => ({ ...state, userId: '', name: {}}),
  [LOGIN_SUCCESS]: (state, { _id, name }) => ({ ...state, userId: _id, name, error: '' }),
}

const loadTokenSSR = (state, ssrCookie) => {
  const { user, isExpired } = getSession(getCookies(ssrCookie))

  return {
    ...state,
    userId: user && !isExpired ? user._id : '',
    name: user && !isExpired ? user.name : {}
  }
}

export default function users(state = initialState, { type, payload }) {
  return handlers[type] ? handlers[type](state, payload) : state
}
