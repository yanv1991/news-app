import React from 'react'
import Slider from "react-slick";
import moment from 'moment'
import { Card, CardContent, CardMedia, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import EditIcon from '@material-ui/icons/Edit';
import Dotdotdot from 'react-dotdotdot'
import { withBreakpoints } from 'react-breakpoints'
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
  card: {
    display: 'flex',
    maxHeight: '171px',
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
  },
  cardGrid: {
    padding: '20px',
  },
  icon: {
    margin: theme.spacing.unit,
    fontSize: 32,
    cursor: 'pointer'
  },
  keepReading: { cursor: 'pointer' },
  progressContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  progress: {
    margin: theme.spacing.unit * 2,
  },
})

const DEFAULT_IMAGE = 'data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22288%22%20height%3D%22225%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20288%20225%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_164edaf95ee%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_164edaf95ee%22%3E%3Crect%20width%3D%22288%22%20height%3D%22225%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2296.32500076293945%22%20y%3D%22118.8%22%3EThumbnail%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E' // eslint-disable-line max-len

function CarouselItem({ onKeepReading, userId, item, classes, onClickItem }) {
  const { name, description, _id, imageUrl } = item

  return (
    <Grid item key={_id} xs={12} md={12} className={classes.cardGrid}>
      <Card className={classes.card} onClick={() => onKeepReading(item)}>
        <Hidden xsDown>
          <CardMedia
            className={classes.cardMedia}
            image={imageUrl || DEFAULT_IMAGE}
            title={name}
          />
        </Hidden>
        <div className={classes.cardDetails}>
          <CardContent>
            <Typography component="h2" variant="h5">
              {name}
            </Typography>
            <Typography variant="subtitle1" color="textSecondary">
              {moment(item.createdAt).format("MMM Do YYYY")}
            </Typography>
            <Typography variant="subtitle1" paragraph>
              <Dotdotdot clamp={2} tagName="span">
                {description}
              </Dotdotdot>
            </Typography>
            <Typography className={classes.keepReading} variant="subtitle1" color="primary" onClick={() => onKeepReading(item)}>
              Continue reading...
            </Typography>
          </CardContent>
        </div>
        {
          userId && (
            <EditIcon onClick={() => onClickItem(item)} className={classes.icon} />
          )
        }
      </Card>
    </Grid>
  )
}

 function MyCarousel({ isFetching, currentBreakpoint, userId, items, onClickItem, classes, isMobile, onKeepReading }) {
   const isMobileRendered = ['mobile', 'mobileLandscape'].includes(currentBreakpoint)
   const settings = {
     dots: true,
     infinite: true,
     speed: 500,
     slidesToShow: isMobileRendered ? 1 : 2,
     slidesToScroll: isMobileRendered ? 1 : 2,
     autoplay: true,
   };

  if(isFetching) return (
    <div className={classes.progressContainer}><CircularProgress className={classes.progress} /></div>
  )

  return (
      <Slider {...settings}>
        {items && items.map((item) => (
          <CarouselItem
            onKeepReading={onKeepReading}
            userId={userId}
            key={item._id} item={item}
            onClickItem={onClickItem}
            classes={classes}
          />
        ))}
      </Slider>
    )
}

export default withBreakpoints(withStyles(styles)(MyCarousel))
