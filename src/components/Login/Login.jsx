import React from 'react';
import { withFormik } from 'formik';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormHelperText from '@material-ui/core/FormHelperText';
import { withStyles } from '@material-ui/core/styles';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  loginDialog: { margin: 0 },
  helperText: { marginTop: 0, color: 'red' },
  textField: {
    [theme.breakpoints.up('sm')]: {
      minWidth: '360px',
    },
  }
});

class FormDialog extends React.Component {
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { fullScreen, errors, classes, handleSubmit, values, handleChange } = this.props

    return (
      <>
        <Button variant="outlined" size="small" onClick={this.handleClickOpen}>
          Login
        </Button>
        <Dialog
          fullScreen={fullScreen}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          className={classes.loginDialog}
        >
          <form onSubmit={handleSubmit}>
            <DialogTitle id="form-dialog-title">Login</DialogTitle>
            <DialogContent>
              <Grid container>
                <Grid item xs={12}>
                  <TextField
                    autoFocus
                    id="email"
                    label="Email Address"
                    type="email"
                    margin="normal"
                    variant="filled"
                    fullWidth
                    value={values.email}
                    onChange={handleChange}
                    className={classes.textField}
                  />
                  <FormHelperText id="component-error-text" className={classes.helperText}>{errors.email}</FormHelperText>
                  <TextField
                    id="password"
                    label="Password"
                    type="password"
                    margin="normal"
                    variant="filled"
                    fullWidth
                    values={values.password}
                    onChange={handleChange}
                    className={classes.textField}
                  />
                  <FormHelperText id="component-error-text" className={classes.helperText}>{errors.password}</FormHelperText>
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                Cancel
              </Button>
              <Button type="submit" color="primary">
                Submit
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </>
    );
  }
}

const withFormEnhancers = withFormik({
  initialValues: () => ({ email: '', password: '' }),

  // Custom sync validation
  validate: values => {
    const errors = {};

    if (!values.email) {
      errors.email = 'Required';
    }

    if (!values.password) {
      errors.password = 'Required';
    }

    return errors;
  },

  handleSubmit: (values, { props: { onLogIn, onShowStatusMessage }}) => {
    const { email, password } = values

    onLogIn({ email, password }).then((props) => {
      const { err, payload } = props

      if(err) {
        onShowStatusMessage({
          position: {
            vertical: 'bottom',
            horizontal: 'left',
          },
          message: err,
          showMessage: true,
          variant: 'error'
        })
      } else {
        const name = `${payload.name.first} ${payload.name.last}`

        onShowStatusMessage({
          position: {
            vertical: 'top',
            horizontal: 'right',
          },
          message: `Welcome ${name}`,
          showMessage: true,
          variant: 'success'
        })
      }
    })
  },

  displayName: 'BasicForm',
})

export default withFormEnhancers(withMobileDialog()(withStyles(styles)(FormDialog)))
