import { GET_NEWS, GET_NEWS_SUCCESS, UPDATE_NEW_SUCCESS, DELETE_NEW_SUCCESS, CREATE_NEW_SUCCESS } from '../constants/ActionTypes'

const initialState = {
  newList: [],
  fetched: false,
  isFetching: false,
}

const handlers = {
  [GET_NEWS]: (state) => ({ ...state, isFetching: true }),
  [GET_NEWS_SUCCESS]: (state, data) => ({ ...state, fetched: true, newList: data, isFetching: false }),
  [UPDATE_NEW_SUCCESS]: (state, data) => ({
    ...state,
    newList: state.newList.map((current) => {
      if (current._id === data._id) return data

      return current
    })
  }),
  [DELETE_NEW_SUCCESS]: (state, data) => ({
    ...state,
    newList: state.newList.filter(({ _id }) => _id !== data)
  }),
  [CREATE_NEW_SUCCESS]: (state, data) => ({
    ...state,
    newList: [...state.newList, data]
  }),
}

export default function news(state = initialState, { type, payload }) {
  return handlers[type] ? handlers[type](state, payload) : state
}
