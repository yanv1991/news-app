import fetch from 'cross-fetch';

import { LOGIN_SUCCESS, LOGOUT_SUCCESS, LOGIN_FAIL, LOAD_AUTH_SSR } from '../constants/ActionTypes'
import { ENDPOINT_URL, AUTH_TOKEN } from '../constants'
import { getCookies, decodeToken } from '../helpers'

function loginSuccess(payload) {
  const cookies = getCookies()
  const { _id, name } = decodeToken(payload.token)

  cookies.set(AUTH_TOKEN, payload.token)
  return {
    type: LOGIN_SUCCESS,
    payload: { _id, name },
  }
}

const loginFail = (err) => ({ type: LOGIN_FAIL, err })

export const loginAuthSSR = (cookies) => {
  return { type: LOAD_AUTH_SSR, payload: { cookies } }
}

export function logout(payload) {
  const cookies = getCookies()

  cookies.remove(AUTH_TOKEN)
  return {
    type: LOGOUT_SUCCESS,
  }
}

export function login(user) {
  return function(dispatch) {
    const formData = new FormData();

    formData.append('email', user.email)
    formData.append('password', user.password)

    return fetch(`${ENDPOINT_URL}/users/auth`, {
        method: 'POST',
        body: formData
      })
      .then((res) => res.json())
      .then(json => {
        if(json.err) return dispatch(loginFail(json.err))

        return dispatch(loginSuccess(json))
      }).catch((err) => { return { err: err.message }})
  }
}
