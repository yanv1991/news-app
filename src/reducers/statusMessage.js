import { SHOW_STATUS_MESSAGE } from '../constants/ActionTypes'

const initialState = {
  variant: 'info',
  showMessage: false,
  message: '',
  position: {
    vertical: 'top',
    horizontal: 'right',
  },
}

const handlers = { [SHOW_STATUS_MESSAGE]: (state, payload) => ({ ...state, ...payload }) }

export default function statusMessage(state = initialState, { type, payload }) {
  return handlers[type] ? handlers[type](state, payload) : state
}
