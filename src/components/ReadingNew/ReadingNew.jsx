import React from 'react';
import Typography from '@material-ui/core/Typography';
import moment from 'moment'
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  date: { fontSize: '12px' }
})

function ReadingNew(props) {
  const { readingNew = {}, classes, name, isFetching } = props;

  if(isFetching) return null

  const author = readingNew.user && readingNew.user.name
  const authorName = author ? `${author.first} ${author.last}` : `${name.first} ${name.last}`

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        {readingNew.name}
      </Typography>
      <Typography variant="subtitle2" gutterBottom className={classes.date}>
        {moment(readingNew.createdAt).format("MMMM D, YYYY")} by {authorName}
      </Typography>
      <Typography>
        {readingNew.description}
      </Typography>
    </React.Fragment>
  );
}

export default withStyles(styles)(ReadingNew);
