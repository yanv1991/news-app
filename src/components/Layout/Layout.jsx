import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import enhancers from './layout.enhancers'
import Header from './Header'
import About from './About'
import Footer from './Footer'

import Carousel from '../Carousel/Carousel'
import Dialog from '../Dialog/Dialog'
import StatusMessage from '../StatusMessage/StatusMessage'
import ReadingNew from '../ReadingNew/ReadingNew'

const styles = theme => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  toolbarMain: {
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    justifyContent: 'space-between',
  },
  mainFeaturedPost: {
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.common.white,
    marginBottom: theme.spacing.unit * 4,
  },
  mainFeaturedPostContent: {
    padding: '2rem',
  },
  mainGrid: {
    marginTop: theme.spacing.unit * 3,
  },
  card: {
    display: 'flex',
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
  },
  markdown: {
    padding: `${theme.spacing.unit * 3}px 0`,
  },
  sidebarAboutBox: {
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.grey[200],
  },
  sidebarSection: {
    marginTop: theme.spacing.unit * 3,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    marginTop: theme.spacing.unit * 8,
    padding: `${theme.spacing.unit * 6}px 0`,
  },
  fab: {
    margin: theme.spacing.unit,
  },
});

function Blog(props) {
  const {
    onCreateItem,
    onDeleteItem,
    onSaveChanges,
    classes,
    news,
    onToggleDialog,
    isDialogOpen,
    onEditNew,
    currentNew,
    onLogIn,
    onLogout,
    users: { userId, name },
    readingNew,
    onKeepReading,
    isMobileRendered,
    archives,
    social,
    onShowStatusMessage,
    isFetchingNews,
  } = props;
  return (
    <React.Fragment>
      <StatusMessage />
      <Dialog
        {...currentNew}
        saveChanges={onSaveChanges}
        showDialog={isDialogOpen}
        toggleDialog={onToggleDialog}
        deleteItem={onDeleteItem}
      />
      <CssBaseline />
      <div className={classes.layout}>
        <Header
          onShowStatusMessage={onShowStatusMessage}
          classes={classes}
          userId={userId}
          onLogout={onLogout}
          onLogIn={onLogIn}
          isMobileRendered={isMobileRendered}
        />
        <main>
          <Grid item xs={12} md={8}>
            {userId && (
              <Fab color="primary" aria-label="Add" className={classes.fab} onClick={onCreateItem}>
                <AddIcon/>
              </Fab>
            )}
          </Grid>
          {/* Sub featured posts */}
          <Carousel isFetching={isFetchingNews} onKeepReading={onKeepReading} items={news} onClickItem={onEditNew} userId={userId} />
          {/* End sub featured posts */}
          <Grid container spacing={40} className={classes.mainGrid}>
            {/* Main content */}
            <Grid item xs={12} md={8}>
              <Typography variant="h6" gutterBottom>
                From the Firehose
              </Typography>
              <Divider />
              <ReadingNew isFetching={isFetchingNews} readingNew={readingNew || news[0]} name={name} />
            </Grid>
            {/* End main content */}
            {/* Sidebar */}
            <Grid item xs={12} md={4}>
              <About classes={classes} />
              <Typography variant="h6" gutterBottom className={classes.sidebarSection}>
                Archives
              </Typography>
              {archives.map(archive => (
                <Typography key={archive}>{archive}</Typography>
              ))}
              <Typography variant="h6" gutterBottom className={classes.sidebarSection}>
                Social
              </Typography>
              {social.map(network => (
                <Typography key={network}>{network}</Typography>
              ))}
            </Grid>
            {/* End sidebar */}
          </Grid>
        </main>
      </div>
      {/* Footer */}
      <Footer classes={classes} />
      {/* End footer */}
    </React.Fragment>
  );
}

Blog.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default enhancers(withStyles(styles)(Blog));
