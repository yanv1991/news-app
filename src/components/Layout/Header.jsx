import React, { Fragment } from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


import ListSection from '../ListSection/ListSection'
import Login from '../Login/Login'

const sections = [
  'Technology',
  'Design',
  'Culture',
  'Business',
  'Politics',
  'Opinion',
  'Science',
  'Health',
  'Style',
  'Travel',
];

function Header({ classes, userId, onLogout, onLogIn, isMobileRendered, onShowStatusMessage }) {
  return (
    <Fragment>
      <Toolbar className={classes.toolbarMain}>
        <Button size="small">Subscribe</Button>
        <Typography
          component="h2"
          variant="h5"
          color="inherit"
          align="center"
          noWrap
          className={classes.toolbarTitle}
        >
          Blog
        </Typography>
        <IconButton>
          <SearchIcon />
        </IconButton>
        {
          userId ? (
            <Button variant="outlined" size="small" onClick={onLogout}>
              Log out
            </Button>
          ) : (
            <Login onLogIn={onLogIn} onShowStatusMessage={onShowStatusMessage} />
          )
        }
      </Toolbar>
      <Toolbar variant="dense" className={classes.toolbarSecondary}>
        {
          isMobileRendered ? (
            <ListSection items={sections.slice(0, 3).concat('More')} />
          ) : (
            sections.map(section => (
              <Typography color="inherit" noWrap key={section}>
                {section}
              </Typography>
            ))
          )
        }
      </Toolbar>
    </Fragment>
  )
}

export default Header
