import { compose } from 'redux'
import { connect } from 'react-redux'
import { lifecycle, withHandlers, withState, withProps } from 'recompose'
import { withBreakpoints } from 'react-breakpoints'

import { fetchNews, updateNew, deleteNew, createNew } from '../../actions/news'
import { login, logout } from '../../actions/users'
import { showStatusMessage } from '../../actions/statusMessage'
import { showSaveMessage } from '../../helpers'

export default compose(
  connect(({ users, news: { isFetching, newList, fetched }}) => ({ users, news: newList, areNewsFetched: fetched, isFetchingNews: isFetching }),
  {
    onFetchNews: fetchNews,
    onCreateNew: createNew,
    onUpdateNew: updateNew,
    onDeleteNew: deleteNew,
    onLogIn: login,
    onLogout: logout,
    onShowStatusMessage: showStatusMessage,
  }
),
  lifecycle({
    componentDidMount() {
      const { areNewsFetched, onFetchNews } = this.props

      if(!areNewsFetched) {
        onFetchNews()
      }
    }
  }),
  withState('isDialogOpen', 'toggleDialog', false),
  withState('currentNew', 'setCurrentNew', {}),
  withState('readingNew', 'onKeepReading'),
  withHandlers({
    onEditNew: ({ toggleDialog, setCurrentNew }) => (item) => {
      setCurrentNew(item)
      toggleDialog((prevState) => !prevState)
    },
    onToggleDialog: ({ toggleDialog }) => () => {
      toggleDialog((prevState) => !prevState)
    },
    onSaveChanges: ({ onShowStatusMessage, users: { userId }, onUpdateNew, onCreateNew }) => (item) => {
      const data = { ...item, userId: userId }

      if(item._id) {
        return onUpdateNew(data).then(({ payload }) => {
          showSaveMessage(onShowStatusMessage, payload)

          return payload
        })
      } else {
        return onCreateNew(data).then(({ payload }) => {
          showSaveMessage(onShowStatusMessage, payload)

          return payload
        })
      }
    },
    onDeleteItem: ({ onShowStatusMessage, toggleDialog, onDeleteNew }) => (itemId) => {
      onDeleteNew(itemId).then(({ payload }) => {
        showSaveMessage(onShowStatusMessage, payload)
      })
      toggleDialog((prevState) => !prevState)
    },
    onCreateItem: ({ toggleDialog, setCurrentNew }) => () => {
      toggleDialog((prevState) => !prevState)
      setCurrentNew({})
    }
  }),
  withBreakpoints,
  withProps(({ currentBreakpoint }) => ({
    archives: [
      'March 2020',
      'February 2020',
      'January 2020',
      'December 2019',
      'November 2019',
      'October 2019',
      'September 2019',
      'August 2019',
      'July 2019',
      'June 2019',
      'May 2019',
      'April 2019',
    ],
    social: ['GitHub', 'Twitter', 'Facebook'],
    isMobileRendered: ['mobile', 'mobileLandscape'].includes(currentBreakpoint),
  })),
)
