import Cookies from 'universal-cookie';
import decode from 'jwt-decode'

import { AUTH_TOKEN } from '../constants'

const cookies = new Cookies();

export const getCookies = (ssrCookie) => {
  if(ssrCookie) return new Cookies(ssrCookie)

  return cookies
}

export const decodeToken = (token) => {
  return decode(token)
}

export const getAuthorization = () => {
  return `Jwt ${cookies.get(AUTH_TOKEN)}`
}

export const isTokenExpired = (exp) => {
  return Boolean((Date.now() / 1000) > exp)
}

export const getSession = (cookies) => {
  const authToken = cookies.get(AUTH_TOKEN)
  const user = authToken && decodeToken(authToken)
  const isExpired = user && isTokenExpired(user.exp)

  return { user, isExpired }
}

export const showSaveMessage = (action, payload) => {
  if(payload) return action({
    position: {
      vertical: 'top',
      horizontal: 'right',
    },
    message: `Changes saved successfully`,
    showMessage: true,
    variant: 'success'
  })

  return action({
    position: {
      vertical: 'bottom',
      horizontal: 'left',
    },
    message: `Something went wrong`,
    showMessage: true,
    variant: 'error'
  })
}
