import news from './news'
import users from './users'
import statusMessage from './statusMessage'

export default { news, users, statusMessage }
