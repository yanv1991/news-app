import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { compose, withHandlers, withState } from 'recompose'
import { withFormik } from 'formik';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import FormHelperText from '@material-ui/core/FormHelperText';
import withMobileDialog from '@material-ui/core/withMobileDialog';

import Dropzone from '../DropZone/DropZone'
import Confirmation from '../Confirmation/Confirmation'

const styles = theme => ({
  textField: {},
  helperText: { marginTop: 0, color: 'red' },
  dropZone: { marginTop: '16px' }
});

const DialogTitle = withStyles(theme => ({
  root: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing.unit * 2,
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing.unit,
    top: theme.spacing.unit,
    color: theme.palette.grey[500],
  },
}))(props => {
  const { children, classes, onClose } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing.unit * 2,
  },
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
  root: {
    borderTop: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing.unit,
  },
}))(MuiDialogActions);

function CustomizedDialog(props) {
  const { fullScreen, errors, _id, onToggleConfirmation, deleteItem, showConfirmation, onChangeImage, classes, showDialog, toggleDialog, handleSubmit, handleChange, values: { name, description }} = props

  if(!showDialog) return null

  return (
    <form>
      <Dialog
        fullScreen={fullScreen}
        onClose={toggleDialog}
        aria-labelledby="customized-dialog-title"
        open={showDialog}
      >
        <DialogTitle id="customized-dialog-title" onClose={toggleDialog}>
          {_id ? 'Edit New': 'Create New'}
        </DialogTitle>
        <DialogContent>
          <Grid container>
            <Grid item xs={12}>
              <TextField
                id="name"
                label="Name"
                className={classes.textField}
                value={name}
                onChange={handleChange}
                margin="normal"
                variant="filled"
                fullWidth
              />
              <FormHelperText id="component-error-text" className={classes.helperText}>{errors.name}</FormHelperText>
              <TextField
                id="description"
                label="Description"
                multiline
                rows="4"
                className={classes.textField}
                margin="normal"
                variant="filled"
                onChange={handleChange}
                value={description}
                fullWidth
              />
              <FormHelperText id="component-error-text" className={classes.helperText}>{errors.description}</FormHelperText>
              <Dropzone className={classes.dropZone} onChangeImage={onChangeImage} />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleSubmit} color="primary">
            Save changes
          </Button>
          {
            _id && (
              <Button onClick={onToggleConfirmation} color="primary">
                Delete
              </Button>
            )
          }
        </DialogActions>
        <Confirmation toggleConfirmation={onToggleConfirmation} showConfirmation={showConfirmation} handleConfirm={deleteItem} id={_id} />
      </Dialog>
    </form>
  );
}

const withFormEnhancers = withFormik({
  enableReinitialize: true,
  initialValues: ({ name = '', description = '' }) => ({ name, description }),

  // Custom sync validation
  validate: values => {
    const errors = {};

    if (!values.name) {
      errors.name = 'Required';
    }

    if (!values.description) {
      errors.description = 'Required';
    }

    return errors;
  },

  handleSubmit: (values, { props: { saveChanges, toggleDialog }}) => {
    saveChanges(values).then((data) => {
      if(data) {
        toggleDialog()
      }
    })
  },

  displayName: 'BasicForm',
})

const withDialogHandlers = withHandlers({
  onChangeImage: ({ setFieldValue }) => (file) => {
    setFieldValue('image', file)
  },
  onToggleConfirmation: ({ toggleConfirmation }) => () => {
    toggleConfirmation((prevState) => !prevState)
  },
})

const enhancedForm = compose(
  withStyles(styles),
  withState('showConfirmation', 'toggleConfirmation', false),
  withFormEnhancers,
  withDialogHandlers
)


export default enhancedForm(withMobileDialog()(CustomizedDialog));
