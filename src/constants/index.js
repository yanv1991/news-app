export const ENDPOINT_URL = 'http://newsservice-env.mvn6j9xwfm.us-east-1.elasticbeanstalk.com' // TOOD: refactor to use ENV VARIABLES
export const AUTH_TOKEN = 'auth_token'
export const breakpoints = {
  mobile: 320,
  mobileLandscape: 480,
  tablet: 768,
  tabletLandscape: 1024,
  desktop: 1200,
  desktopLarge: 1500,
  desktopWide: 1920,
}
