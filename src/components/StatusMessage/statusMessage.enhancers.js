import { connect } from 'react-redux'


export default connect(({ statusMessage: { variant, showMessage, message, position }}) => ({
  variant,
  showMessage,
  message,
  position,
}))
