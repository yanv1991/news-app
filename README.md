This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### Execution
YARN: yarn install && yarn start
NPM: npm install && npm start

### Access the app via AWS
You can access the app deployed by AWS using this url: http://newsapp-env-1.mvn6j9xwfm.us-east-1.elasticbeanstalk.com/


### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the complete read me about [Create React App] here(https://github.com/facebook/create-react-app).

### Run server side mode

Run `npm run server` so the add will build and run in SSR mode the initial request will load in the server :)

### Connect with API

Run the 'news-service' with `npm run start` and change the `ENDPOINT_URL` constants ('src/constants') based on the port selected where the API is running. By default the api runs in port 3001.
Finally run `yarn start` or `npm start` to run the news-app. A .env required file in the `news-service` will be provided for the api to run this project, they should be place in the root folder.

### Database

A Database was created in mongo cloud, you can install and run Mongo Compass community tool ((https://docs.mongodb.com/compass/master/install/)) to see the database collections.

### Users
This is the list of preconfigured users in the DB:
email: yanv1991@gmail.com pass: test
email: johndoe@gmail.com pass: 123
