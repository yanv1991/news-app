import React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

function About ({ classes }) {
  return (
    <Paper elevation={0} className={classes.sidebarAboutBox}>
      <Typography variant="h6" gutterBottom>
        About
      </Typography>
      <Typography>
        Etiam porta sem malesuada magna mollis euismod. Cras mattis consectetur purus sit
        amet fermentum. Aenean lacinia bibendum nulla sed consectetur.
      </Typography>
    </Paper>
  )
}

export default About
