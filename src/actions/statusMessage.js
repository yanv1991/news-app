import { SHOW_STATUS_MESSAGE } from '../constants/ActionTypes'

export function showStatusMessage(payload) {
  return {
    type: SHOW_STATUS_MESSAGE,
    payload,
  }
}
